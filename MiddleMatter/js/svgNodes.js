function formatNode()
{
    var margins = {topbottom:5, leftright:4}

    var node = d3.select(this);
    var rect = node.select("rect");
    node.select("text")
        .each(function(){
            var box = this.getBBox();
            rect.attr("width",box.width + 2*margins.leftright)
                .attr("height",box.height + 2*margins.topbottom)
            d3.select(this)
              .attr("x", -box.x+margins.leftright)
              .attr("y", -box.y+margins.topbottom);
        });
    rect.attr("fill","#FFFFFF");
}

function initNode()
{
    var node = d3.select(this);
    node.append("rect")
        .attr("rx", 5).attr("ry", 5).attr("height", 17).attr("width",150)
        .attr("stroke", "#000000");      
    node.append("text")
        .attr("text-anchor", "middle").attr("dy", 1.1 / 2 + 0.5 + 'em');
}


    //var colorings = [
    //    { regex:/^[0-9]+$/ , color:"#7fc97f"},
    //    { regex:/^[*+-\/]$/, color:"#beaed4"},
    //    { regex:/^[\(\)]$/ , color:"#fdc086"},
    //]


    //nodes.select("text").text(String);
    //nodes.select("rect").attr("fill", function(d){
    //    // Array.some() simulates "break from foreach" loop functionality
    //    // retcolor is closured for return value 
    //    var retcolor = "#c7c4df";
    //    colorings.some(function(col){ 
    //        if (!col.regex.test(d)) return false;
    //        retcolor = col.color;
    //        return true;
    //    }); 
    //    return retcolor;
    //});

